import { DemoViewchildPage } from './app.po';

describe('demo-viewchild App', () => {
  let page: DemoViewchildPage;

  beforeEach(() => {
    page = new DemoViewchildPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
